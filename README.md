# Skeinan Netcopy

This application is used to transfer entire folders between computers in a network using TLS with a local certificate.

## ⚠️ Warning

Transferring files may take a great fraction of the network power to make it faster, which may cause other network actions in the computer to be delayed or stopped.

## Development Instructions

The Skeinan NuGet package source needs to be added to install the `skeinan-wpf` package:
https://pkgs.dev.azure.com/skeinan/Skeinan/_packaging/skeinan/nuget/v3/index.json

## Application Usage Instructions

The application should be used as a server in the computer that is sending the files and as a client in the computer that is receiving the files.

In `Properties/Settings.settings`, the entries `IsServerEnabled` and `IsClientEnabled` should be used to define if the current execution is for the server or client.

Both server and client options can be enabled at the same time, which causes the application to load two separate windows and allows transferring files in the same computer. This is only useful for testing.
