﻿using System.ComponentModel;
using System.Security;

namespace Skeinan.Netcopy.Connection {
	public class ConnectionHandler : Service {
		private string mUsername;
		public string Username {
			get { return mUsername; }
			set { mUsername = value; NotifyPropertyChanged(); }
		}

		private SecureString mPassword;
		public SecureString Password {
			get { return mPassword; }
			set { mPassword = value; NotifyPropertyChanged(); }
		}

		private string mStatus;
		public string Status {
			get { return mStatus; }
			set { mStatus = value; NotifyPropertyChanged(); }
		}

		private string mConnection;
		public string Connection {
			get { return mConnection; }
			set { mConnection = value; NotifyPropertyChanged(); }
		}

		private BindingList<string> mErrorMessages = new BindingList<string>();
		public BindingList<string> ErrorMessages {
			get { return mErrorMessages; }
			set { mErrorMessages = value; NotifyPropertyChanged(); }
		}

		protected const string CERTIFICATE_NAME = "Skeinan";
		protected SkeinanLogger Logger { get; }
		protected SyncStorageUtil StorageUtil { get; }

		public event TcpEventHandler Connected;

		public ConnectionHandler(SkeinanLogger logger, SyncStorageUtil storageUtil) {
			Logger = logger;
			StorageUtil = storageUtil;
		}

		protected void OnConnected(NetworkStreamWrapper stream) {
			Connected(this, new TcpEventArgs(stream));
		}
	}
}
