﻿using System;
using System.Security;

namespace Skeinan.Netcopy.Connection {
	public class AuthenticationEventArgs : EventArgs {
		public string Username { get; set; }
		public SecureString Password { get; set; }

		public AuthenticationEventArgs() {
		}
	}
}
