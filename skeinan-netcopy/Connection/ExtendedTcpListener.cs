﻿using System.Net;
using System.Net.Sockets;

namespace Skeinan.Netcopy.Connection {
	public class ExtendedTcpListener : TcpListener {
		public new bool Active {
			get {
				return base.Active;
			}
		}

		public ExtendedTcpListener(IPEndPoint localEP) : base(localEP) {
		}

		public ExtendedTcpListener(IPAddress localaddr, int port) : base(localaddr, port) {
		}
	}
}
