﻿using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Operators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace Skeinan.Netcopy.Connection {
	public class CertificateManager {
		public X509Certificate2 Create(string commonName) {
			AsymmetricCipherKeyPair keyPair = GenerateRsaKeyPair(2048);
			Org.BouncyCastle.X509.X509Certificate certificate = GenerateCertificate(commonName, keyPair);
			return GeneratePfx(commonName, certificate, keyPair.Private);
		}

		private AsymmetricCipherKeyPair GenerateRsaKeyPair(int length) {
			KeyGenerationParameters keygenParam = new KeyGenerationParameters(new SecureRandom(), length);
			RsaKeyPairGenerator keyGenerator = new RsaKeyPairGenerator();
			keyGenerator.Init(keygenParam);
			return keyGenerator.GenerateKeyPair();
		}

		private RSACryptoServiceProvider ConvertKeyPairToPrivateKey(AsymmetricKeyEntry keyEntry) {
			RSACryptoServiceProvider intermediateProvider = (RSACryptoServiceProvider)DotNetUtilities.ToRSA((RsaPrivateCrtKeyParameters)keyEntry.Key);
			RSACryptoServiceProvider csp = new RSACryptoServiceProvider(new CspParameters(1, "Microsoft Strong Cryptographic Provider", new Guid().ToString(), new System.Security.AccessControl.CryptoKeySecurity(), null));
			csp.ImportCspBlob(intermediateProvider.ExportCspBlob(true));
			return csp;
		}

		private Org.BouncyCastle.X509.X509Certificate GenerateCertificate(string commonName, AsymmetricCipherKeyPair keyPair) {
			ISignatureFactory signatureFactory = new Asn1SignatureFactory(PkcsObjectIdentifiers.Sha256WithRsaEncryption.ToString(), keyPair.Private);
			X509Name commonNameDN = new X509Name("CN=" + commonName);

			X509V3CertificateGenerator certGenerator = new X509V3CertificateGenerator();
			certGenerator.SetIssuerDN(commonNameDN);
			certGenerator.SetSubjectDN(commonNameDN);
			certGenerator.SetSerialNumber(BigInteger.ValueOf(1));
			certGenerator.SetNotAfter(DateTime.UtcNow.AddHours(1));
			certGenerator.SetNotBefore(DateTime.UtcNow);
			certGenerator.SetPublicKey(keyPair.Public);
			return certGenerator.Generate(signatureFactory);
		}

		private X509Certificate2 GeneratePfx(string commonName, Org.BouncyCastle.X509.X509Certificate certificate, AsymmetricKeyParameter privateKey) {
			Pkcs12Store store = new Pkcs12StoreBuilder().Build();
			X509CertificateEntry certEntry = new X509CertificateEntry(certificate);
			store.SetCertificateEntry(certificate.SubjectDN.ToString(), certEntry);
			AsymmetricKeyEntry keyEntry = new AsymmetricKeyEntry(privateKey);
			store.SetKeyEntry(certificate.SubjectDN.ToString() + "_key", keyEntry, new X509CertificateEntry[] { certEntry });
			using (MemoryStream stream = new MemoryStream()) {
				store.Save(stream, null, new SecureRandom());
				return new X509Certificate2(stream.ToArray()) {
					PrivateKey = ConvertKeyPairToPrivateKey(keyEntry),
					FriendlyName = commonName
				};
			}
		}
	}
}
