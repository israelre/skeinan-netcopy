﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace Skeinan.Netcopy.Connection {
	public class NetworkStreamWrapper {
		public static Encoding Encoding { get; } = Encoding.UTF8;
		public SkeinanLogger Logger { get; set; }
		private const int BUFFER_SIZE = 1048576;

		protected Stream Stream { get; }

		public static byte ConvertToByte(bool value) {
			return Convert.ToByte(value);
		}

		public static byte[] ConvertToBytes(long value) {
			return BitConverter.GetBytes(value);
		}

		public static byte[] ConvertToBytes(string value) {
			return Encoding.GetBytes(value + "\0");
		}

		public NetworkStreamWrapper(Stream stream, SkeinanLogger logger) {
			Stream = stream;
			Logger = logger;
		}

		public bool ReadBool() {
			return ReadByte() != 0;
		}

		public void WriteBool(bool value) {
			WriteByte(ConvertToByte(value));
		}

		public byte ReadByte() {
			return ReadBytes(1)[0];
		}

		public void WriteByte(byte value) {
			Stream.WriteByte(value);
		}

		public byte[] ReadBytes(int byteCount) {
			int remainingByteCount = byteCount;
			byte[] buffer = new byte[byteCount];
			while (remainingByteCount > 0) {
				remainingByteCount -= Stream.Read(buffer, byteCount - remainingByteCount, remainingByteCount);
			}
			return buffer;
		}

		public bool CompareSecureString(SecureString secureString) {
			List<byte> bytes = new List<byte>();
			for (byte nextByte = ReadByte(); nextByte != 0; nextByte = ReadByte()) {
				bytes.Add(nextByte);
			}
			return bytes.SequenceEqual(ToByteArray(secureString));
		}

		public void WriteSecureString(SecureString secureString) {
			byte[] buffer = ToByteArray(secureString);
			Stream.Write(buffer, 0, buffer.Length);
			Stream.WriteByte(0);
		}

		private byte[] ToByteArray(SecureString secureString) {
			IntPtr unmanagedString = IntPtr.Zero;
			try {
				unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(secureString);
				return Encoding.GetBytes(Marshal.PtrToStringUni(unmanagedString));
			} finally {
				if (unmanagedString != IntPtr.Zero) {
					Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
				}
			}
		}

		public int ReadInt() {
			return BitConverter.ToInt32(ReadBytes(sizeof(int)), 0);
		}

		public void WriteInt(int data) {
			byte[] sizeBytes = BitConverter.GetBytes(data);
			Stream.Write(sizeBytes, 0, sizeBytes.Length);
		}

		public long ReadLong() {
			return BitConverter.ToInt64(ReadBytes(sizeof(long)), 0);
		}

		public void WriteLong(long value) {
			byte[] bytes = ConvertToBytes(value);
			Stream.Write(bytes, 0, bytes.Length);
		}

		public string ReadString() {
			List<byte> bytes = new List<byte>();
			for (byte nextByte = ReadByte(); nextByte != 0; nextByte = ReadByte()) {
				bytes.Add(nextByte);
			}
			return Encoding.GetString(bytes.ToArray());
		}

		public void WriteString(string value) {
			byte[] bytes = ConvertToBytes(value);
			Stream.Write(bytes, 0, bytes.Length);
		}

		public void WriteBytes(byte[] bytes) {
			Stream.Write(bytes, 0, bytes.Length);
		}

		public long WriteFile(string sourceFilePath) {
			byte[] fileBytes = File.ReadAllBytes(sourceFilePath);
			long size = fileBytes.LongLength;
			WriteLong(size);
			Stream.Write(fileBytes, 0, fileBytes.Length);
			return size;
		}

		public void ReadFile(string targetFilePath, Action<int> progressCallback) {
			long size = ReadLong();
			long pendingSize = size;
			new FileInfo(targetFilePath).Directory.Create();
			using (FileStream fileStream = File.OpenWrite(targetFilePath)) {
				byte[] buffer = new byte[BUFFER_SIZE];
				while (pendingSize > 0) {
					int receivedSize = Stream.Read(buffer, 0, (int)Math.Min(pendingSize, buffer.Length));
					fileStream.Write(buffer, 0, receivedSize);
					progressCallback?.Invoke(receivedSize);
					pendingSize -= receivedSize;
				}
			}
		}

		public void SkipFile(Action<int> progressCallback) {
			long size = ReadLong();
			long pendingSize = size;
			byte[] buffer = new byte[BUFFER_SIZE];
			while (pendingSize > 0) {
				int receivedSize = Stream.Read(buffer, 0, (int)Math.Min(pendingSize, buffer.Length));
				progressCallback?.Invoke(receivedSize);
				pendingSize -= receivedSize;
			}
		}
	}
}
