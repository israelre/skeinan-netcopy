﻿using System;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace Skeinan.Netcopy.Connection {
	public class ServerConnectionHandler : ConnectionHandler {
		private readonly object mConnectionThreadLock = new object();
		private Thread mConnectionThread;
		private readonly object mListenerLock = new object();
		protected ExtendedTcpListener TcpListener { get; set; }

		public ServerConnectionHandler(SkeinanLogger logger, SyncStorageUtil storageUtil) : base(logger, storageUtil) {
		}

		public bool IsListening() {
			lock (mListenerLock) {
				return IsListening(TcpListener);
			}
		}

		private bool IsListening(ExtendedTcpListener listener) {
			return listener != null && listener.Active;
		}

		public void Listen(string host, string port, string username, SecureString password) {
			try {
				ErrorMessages.Clear();
				lock (mListenerLock) {
					if (IsListening(TcpListener)) {
						ErrorMessages.Add("Server is already active.");
					} else {
						IPAddress address = IPAddress.Parse(host);
						IPEndPoint listenerServer = new IPEndPoint(address, int.Parse(port));
						TcpListener = new ExtendedTcpListener(listenerServer);
						Username = username;
						Password = password;
						TcpListener.Start();
						Task<TcpClient> clientTask = TcpListener.AcceptTcpClientAsync();
						clientTask.ContinueWith(AcceptClient);
					}
				}
			} catch (Exception ex) {
				Logger.LogError(ex);
				ErrorMessages.Add("Failure in server setup.");
			} finally {
				lock (mListenerLock) {
					Status = TcpListener?.Active == true ? "Listening" : "Disconnected";
				}
			}
		}

		public void Disable() {
			try {
				lock (mListenerLock) {
					if (IsListening(TcpListener)) {
						TcpListener.Stop();
						TcpListener = null;
					} else {
						ErrorMessages.Add("Server is not active.");
					}
				}
			} catch (Exception ex) {
				Logger.LogError(ex);
				ErrorMessages.Add("Failure disabling server.");
			}
		}

		private void AcceptClient(Task<TcpClient> clientTask) {
			try {
				ErrorMessages.Clear();
				lock (mConnectionThreadLock) {
					mConnectionThread = Thread.CurrentThread;
				}
				if (!clientTask.IsFaulted) {
					using (clientTask.Result) {
						Status = "Connected";
						Connection = clientTask.Result.Client.RemoteEndPoint.ToString();
						using (SslStream sslStream = new SslStream(clientTask.Result.GetStream(), false)) {
							X509Certificate2 certificate = StorageUtil.LoadServerCertificate(CERTIFICATE_NAME);
							if (certificate == null) {
								certificate = new CertificateManager().Create(CERTIFICATE_NAME);
								StorageUtil.SaveServerCertificate(certificate);
							}
							sslStream.AuthenticateAsServer(certificate, false, SslProtocols.Tls12, true);
							NetworkStreamWrapper stream = new NetworkStreamWrapper(sslStream, Logger);
							bool authRequired = Username != null || Password != null;
							bool authSuccess = true;
							stream.WriteBool(authRequired);
							if (authRequired) {
								string username = stream.ReadString();
								bool isValidPassword = stream.CompareSecureString(Password);
								authSuccess = (Username ?? "").Equals(username) && isValidPassword;
								stream.WriteBool(authSuccess);
							}
							if (authSuccess) {
								OnConnected(stream);
							} else {
								stream.WriteString("Invalid login credentials.");
								ErrorMessages.Add("Invalid login credentials.");
							}
						}
					}
				}
			} catch (ThreadAbortException ex) {
				Logger.LogInfo(ex);
				ErrorMessages.Add("Connection interrupted.");
			} catch (Exception ex) {
				Logger.LogError(ex);
				ErrorMessages.Add("Failure during connection.");
			} finally {
				mConnectionThread = null;
				lock (mListenerLock) {
					Status = TcpListener?.Active == true ? "Listening" : "Disconnected";
					Connection = null;
					if (TcpListener != null) {
						clientTask = TcpListener.AcceptTcpClientAsync();
						clientTask.ContinueWith(AcceptClient);
					}
				}
			}
		}

		public void Stop() {
			ErrorMessages.Clear();
			lock (mConnectionThreadLock) {
				if (mConnectionThread != null) {
					mConnectionThread.Abort();
				} else {
					ErrorMessages.Add("No execution in progress.");
				}
			}
		}
	}
}
