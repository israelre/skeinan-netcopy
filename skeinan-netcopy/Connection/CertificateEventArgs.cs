﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace Skeinan.Netcopy.Connection {
	public class CertificateEventArgs : EventArgs {
		public X509Certificate2 Certificate { get; }
		public bool IsCertificateAccepted { get; set; }

		public CertificateEventArgs(X509Certificate2 certificate) {
			Certificate = certificate;
		}
	}
}
