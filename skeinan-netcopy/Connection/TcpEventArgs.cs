﻿using System;

namespace Skeinan.Netcopy.Connection {
	public class TcpEventArgs : EventArgs {
		public NetworkStreamWrapper Stream { get; }

		public TcpEventArgs(NetworkStreamWrapper stream) {
			Stream = stream;
		}
	}
}
