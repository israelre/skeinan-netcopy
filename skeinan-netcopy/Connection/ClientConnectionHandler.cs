﻿using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Skeinan.Netcopy.Connection {
	public class ClientConnectionHandler : ConnectionHandler {
		public event AuthenticationEventHandler AuthenticationRequired;
		public event CertificateEventHandler CertificateChanged;

		public ClientConnectionHandler(SkeinanLogger logger, SyncStorageUtil storageUtil) : base(logger, storageUtil) {
		}

		public void Connect(string host, string port) {
			try {
				ErrorMessages.Clear();
				if (!string.IsNullOrWhiteSpace(host) && !string.IsNullOrWhiteSpace(port)) {
					TcpClient client = new TcpClient();
					Task clientTask = client.ConnectAsync(host, int.Parse(port));
					clientTask.ContinueWith(task => HandleConnection(client));
				}
			} catch (Exception ex) {
				Logger.LogError(ex);
				ErrorMessages.Add("Failure in connection attempt.");
			}
		}

		private void HandleConnection(TcpClient client) {
			try {
				ErrorMessages.Clear();
				using (client) {
					Status = GetState(client).ToString();
					Connection = client.Client.RemoteEndPoint.ToString();
					using (SslStream sslStream = new SslStream(client.GetStream(), false, RemoteCertificateValidation)) {
						sslStream.AuthenticateAsClient(CERTIFICATE_NAME, null, SslProtocols.Tls12, true);
						NetworkStreamWrapper stream = new NetworkStreamWrapper(sslStream, Logger);
						bool authSuccess = true;
						if (stream.ReadBool()) {
							AuthenticationEventArgs eventArgs = new AuthenticationEventArgs();
							AuthenticationRequired(this, eventArgs);
							stream.WriteString(eventArgs.Username);
							stream.WriteSecureString(eventArgs.Password);
							authSuccess = stream.ReadBool();
						}
						if (authSuccess) {
							OnConnected(stream);
						} else {
							ErrorMessages.Add(stream.ReadString());
						}
					}
				}
				Status = null;
				Connection = null;
			} catch (Exception ex) {
				Logger.LogError(ex);
				ErrorMessages.Add("Failure during connection.");
			}
		}

		public bool RemoteCertificateValidation(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
			try {
				bool isAcceptableError = sslPolicyErrors == SslPolicyErrors.None || sslPolicyErrors == SslPolicyErrors.RemoteCertificateChainErrors;
				X509ChainElement chainElement = chain?.ChainElements?.Count == 1 ? chain.ChainElements[0] : null;
				X509ChainStatus chainStatus = chainElement?.ChainElementStatus?.Count() == 1 ? chainElement.ChainElementStatus[0] : new X509ChainStatus();
				bool isAcceptableChainStatus = chainStatus.Status == X509ChainStatusFlags.UntrustedRoot || chainStatus.Status == X509ChainStatusFlags.NoError;
				bool validated = false;
				if (isAcceptableError && isAcceptableChainStatus) {
					X509Certificate2 remoteCertificate = new X509Certificate2(certificate);
					X509Certificate2 localCertificate = StorageUtil.LoadTrustedCertificate(remoteCertificate);
					if (localCertificate != null) {
						validated = true;
					} else {
						CertificateEventArgs eventArgs = new CertificateEventArgs(remoteCertificate);
						CertificateChanged(this, eventArgs);
						if (eventArgs.IsCertificateAccepted) {
							StorageUtil.SaveTrustedCertificate(remoteCertificate);
							validated = true;
						}
					}
				}
				return validated;
			} catch (Exception ex) {
				Logger.LogError(ex);
				return false;
			}
		}

		private TcpState GetState(TcpClient tcpClient) {
			EndPoint localEndPoint = tcpClient.Client?.LocalEndPoint;
			TcpConnectionInformation[] connectionInfos = IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpConnections();
			TcpConnectionInformation connectionInfo = connectionInfos.SingleOrDefault(conn => conn.LocalEndPoint.Equals(localEndPoint));
			return connectionInfo?.State ?? TcpState.Unknown;
		}
	}
}
