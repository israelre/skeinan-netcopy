﻿using System;
using System.Diagnostics;

namespace Skeinan.Netcopy {
	public class SkeinanLogger {
		private TraceSource mTraceSource;

		public SkeinanLogger(string fileName) {
#if DEBUG
			SourceLevels logLevel = SourceLevels.All;
#else
			SourceLevels logLevel = SourceLevels.All;
#endif
			mTraceSource = new TraceSource("Skeinan", logLevel);
			mTraceSource.Listeners.Add(new ConsoleTraceListener());
			mTraceSource.Listeners.Add(new TextWriterTraceListener(fileName));
		}

		public void LogWarning(object data) {
			mTraceSource.TraceData(TraceEventType.Warning, 0, HandleMessage(data));
		}

		public void LogError(object data) {
			mTraceSource.TraceData(TraceEventType.Error, 0, HandleMessage(data));
		}

		public void LogInfo(object data) {
			mTraceSource.TraceData(TraceEventType.Information, 0, HandleMessage(data));
		}

		public void LogDebug(object data) {
			mTraceSource.TraceData(TraceEventType.Verbose, 0, HandleMessage(data));
		}

		private string HandleMessage(object data) {
			return string.Format("{0} {1}", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff"), data);
		}
	}
}
