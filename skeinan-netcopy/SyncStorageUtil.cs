﻿using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace Skeinan.Netcopy {
	public class SyncStorageUtil {
		public string ServerCertificatePath {
			get {
				return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Certificates");
			}
		}

		public string TrustedCertificatePath {
			get {
				return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Certificates/Trusted");
			}
		}

		public string GetBackupLogPath(string backupPath, DateTime startTime) {
			return Path.Combine(backupPath, startTime.ToString("yyyyMMddHHmmss"));
		}

		private string GetBackupLogPath(string backupPath, string fileName, DateTime startTime) {
			return Path.Combine(GetBackupLogPath(backupPath, startTime), fileName);
		}

		public string GetBackupOutputPath(string backupPath) {
			return Path.Combine(backupPath, "Backup");
		}

		public string GetBackupAddedLogPath(string backupPath, DateTime startTime) {
			return GetBackupLogPath(backupPath, "Added.log", startTime);
		}

		public string GetBackupChangedLogPath(string backupPath, DateTime startTime) {
			return GetBackupLogPath(backupPath, "Changed.log", startTime);
		}

		public string GetBackupRemovedLogPath(string backupPath, DateTime startTime) {
			return GetBackupLogPath(backupPath, "Removed.log", startTime);
		}

		public string GetBackupTrashPath(string backupPath, DateTime startTime) {
			return GetBackupLogPath(backupPath, "Trash", startTime);
		}

		public X509Certificate2 LoadTrustedCertificate(X509Certificate2 serverCertificate) {
			if (Directory.Exists(TrustedCertificatePath)) {
				foreach (string file in Directory.EnumerateFiles(TrustedCertificatePath, "*", SearchOption.AllDirectories)) {
					X509Certificate2 certificate = new X509Certificate2(file);
					string serverCertName = serverCertificate.GetNameInfo(X509NameType.SimpleName, false);
					string localCertName = serverCertificate.GetNameInfo(X509NameType.SimpleName, false);
					if (serverCertName?.Equals(localCertName) == true && serverCertificate?.Thumbprint?.Equals(certificate.Thumbprint) == true) {
						return certificate;
					}
				}
			}
			return null;
		}

		public void SaveTrustedCertificate(X509Certificate2 certificate) {
			new DirectoryInfo(TrustedCertificatePath).Create();
			for (int i = 0; i < 10000; ++i) {
				string fileName = certificate.GetNameInfo(X509NameType.SimpleName, false) + i.ToString("0000") + ".crt";
				string path = Path.Combine(TrustedCertificatePath, fileName);
				if (!File.Exists(path)) {
					File.WriteAllBytes(path, certificate.Export(X509ContentType.Cert));
					break;
				}
			}
		}

		public X509Certificate2 LoadServerCertificate(string name) {
			string path = Path.Combine(ServerCertificatePath, name + ".pfx");
			if (File.Exists(path)) {
				return new X509Certificate2(path);
			} else {
				return null;
			}
		}

		public void SaveServerCertificate(X509Certificate2 certificate) {
			new DirectoryInfo(ServerCertificatePath).Create();
			string path = Path.Combine(ServerCertificatePath, certificate.GetNameInfo(X509NameType.SimpleName, false) + ".pfx");
			File.WriteAllBytes(path, certificate.Export(X509ContentType.Pfx));
		}
	}
}
