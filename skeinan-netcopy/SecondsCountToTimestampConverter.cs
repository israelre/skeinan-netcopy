﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Skeinan.Netcopy {
	public class SecondsCountToTimestampConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			double? doubleValue = value as double?;
			string result = null;
			if (doubleValue != null) {
				TimeSpan timeSpan = TimeSpan.FromSeconds(Math.Ceiling((double)doubleValue));
				result = timeSpan.ToString(@"hh\:mm\:ss");
			}
			return result;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}
