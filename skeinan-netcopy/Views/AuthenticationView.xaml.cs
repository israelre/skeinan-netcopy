﻿using SkeinanWpf;
using System.Security;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Skeinan.Netcopy.Views {
	public partial class AuthenticationView : Window {
		public static readonly DependencyProperty UsernameProperty =
				DependencyProperty.Register(nameof(Username), typeof(string), typeof(AuthenticationView));
		public string Username {
			get { return (string)GetValue(UsernameProperty); }
			set { SetValue(UsernameProperty, value); }
		}

		public static readonly DependencyProperty IsConfirmedProperty =
				DependencyProperty.Register(nameof(IsConfirmed), typeof(bool), typeof(AuthenticationView));
		public bool IsConfirmed {
			get { return (bool)GetValue(IsConfirmedProperty); }
			set { SetValue(IsConfirmedProperty, value); }
		}

		public SecureString Password { get; set; }

		public AuthenticationView() {
			WindowStartupLocation = WindowStartupLocation.CenterOwner;
			InitializeComponent();
			new BindingManager(UsernameTextBox, TextBox.TextProperty, this, nameof(Username)).Bind();
			OkButton.Click += OkButton_Click;
			UsernameTextBox.KeyDown += TextBox_KeyDown;
			PasswordTextBox.KeyDown += TextBox_KeyDown;
			this.Activated += AuthenticationView_Activated;
		}

		private void TextBox_KeyDown(object sender, KeyEventArgs e) {
			if (e.Key == Key.Return) {
				Confirm();
			}
		}

		private void AuthenticationView_Activated(object sender, System.EventArgs e) {
			Keyboard.Focus(UsernameTextBox);
		}

		private void OkButton_Click(object sender, RoutedEventArgs e) {
			Confirm();
		}

		private void Confirm() {
			IsConfirmed = true;
			Username = Username ?? "";
			Password = PasswordTextBox.SecurePassword;
			Close();
		}
	}
}
