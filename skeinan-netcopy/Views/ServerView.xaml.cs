﻿using Skeinan.Netcopy.Connection;
using Skeinan.Netcopy.Transfer;
using SkeinanWpf;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using FolderBrowserDialog = System.Windows.Forms.FolderBrowserDialog;
using DialogResult = System.Windows.Forms.DialogResult;
using Skeinan.Netcopy.Properties;
using SkeinanWpf.Converters;
using System.Security;

namespace Skeinan.Netcopy.Views {
	public partial class ServerView : Window {
		public static readonly DependencyProperty SourceFolderProperty =
				DependencyProperty.Register(nameof(SourceFolder), typeof(string), typeof(ServerView));
		public string SourceFolder {
			get { return (string)GetValue(SourceFolderProperty); }
			set { SetValue(SourceFolderProperty, value); }
		}

		public static readonly DependencyProperty HostProperty =
				DependencyProperty.Register(nameof(Host), typeof(string), typeof(ServerView));
		public string Host {
			get { return (string)GetValue(HostProperty); }
			set { SetValue(HostProperty, value); }
		}

		public static readonly DependencyProperty PortProperty =
				DependencyProperty.Register(nameof(Port), typeof(string), typeof(ServerView));
		public string Port {
			get { return (string)GetValue(PortProperty); }
			set { SetValue(PortProperty, value); }
		}

		public static readonly DependencyProperty ErrorMessagesProperty =
				DependencyProperty.Register(nameof(ErrorMessages), typeof(IEnumerable<string>), typeof(ServerView));
		public IEnumerable<string> ErrorMessages {
			get { return (IEnumerable<string>)GetValue(ErrorMessagesProperty); }
			set { SetValue(ErrorMessagesProperty, value); }
		}

		public static readonly DependencyProperty IsAuthenticationEnabledProperty =
				DependencyProperty.Register(nameof(IsAuthenticationEnabled), typeof(bool), typeof(ServerView));
		public bool IsAuthenticationEnabled {
			get { return (bool)GetValue(IsAuthenticationEnabledProperty); }
			set { SetValue(IsAuthenticationEnabledProperty, value); }
		}

		public static readonly DependencyProperty IsRunningProperty =
				DependencyProperty.Register(nameof(IsRunning), typeof(bool), typeof(ServerView));
		public bool IsRunning {
			get { return (bool)GetValue(IsRunningProperty); }
			set { SetValue(IsRunningProperty, value); }
		}

		public static readonly DependencyProperty IsListeningProperty =
				DependencyProperty.Register(nameof(IsListening), typeof(bool), typeof(ServerView));
		public bool IsListening {
			get { return (bool)GetValue(IsListeningProperty); }
			set { SetValue(IsListeningProperty, value); }
		}

		protected SkeinanLogger Logger { get; }
		protected SyncStorageUtil StorageUtil { get; }

		public ServerConnectionHandler Server { get; }
		public ServerTransferManager TransferManager { get; }

		public ServerView(SkeinanLogger logger, SyncStorageUtil storageUtil) {
			Logger = logger;
			StorageUtil = storageUtil;
			SourceFolder = Settings.Default.SourceFolder;
			Host = Settings.Default.ServerHost;
			Port = Settings.Default.ServerPort;
			Server = new ServerConnectionHandler(logger, storageUtil);
			TransferManager = new ServerTransferManager(logger);
			TransferManager.IsMd5Enabled = Settings.Default.IsMd5ComparisonEnabled;
			Server.Connected += Server_Connected;

			Server.ErrorMessages.ListChanged += ErrorMessages_ListChanged;
			TransferManager.ErrorMessages.ListChanged += ErrorMessages_ListChanged;

			InitializeComponent();
			UpdateBindings();
			UpdateActions();
		}

		private void ErrorMessages_ListChanged(object sender, ListChangedEventArgs e) {
			Dispatcher.Invoke(MergeErrorMessages);
		}

		private void MergeErrorMessages() {
			ErrorMessages = Server.ErrorMessages.Concat(TransferManager.ErrorMessages);
		}

		private void UpdateBindings() {
			new BindingManager(ListenHostTextBox, TextBox.TextProperty, this, nameof(Host)).Bind();
			new BindingManager(ListenPortTextBox, TextBox.TextProperty, this, nameof(Port)).Bind();
			new BindingManager(ConnectionStatusLabel, Label.ContentProperty, Server, nameof(Server.Status)).Bind();
			new BindingManager(ConnectionLabel, Label.ContentProperty, Server, nameof(Server.Connection)).Bind();
			new BindingManager(AuthenticationCheckBox, CheckBox.IsCheckedProperty, this, nameof(IsAuthenticationEnabled)).Bind();
			new BindingManager(LogLabel, Label.ContentProperty, this, nameof(ErrorMessages)) {
				Converter = new ListToStringConverter()
			}.Bind();
			new BindingManager(TransferStatusLabel, Label.ContentProperty, TransferManager, nameof(TransferManager.Status)).Bind();
			new BindingManager(ProgressLabel, Label.ContentProperty, TransferManager, nameof(TransferManager.Progress)) {
				Converter = new DoubleToStringConverter("%")
			}.Bind();
			new BindingManager(TransferSpeedLabel, Label.ContentProperty, TransferManager, nameof(TransferManager.MegaBytePerSecond)) {
				Converter = new DoubleToStringConverter(" MB/s")
			}.Bind();
			new BindingManager(RemainingTimeLabel, Label.ContentProperty, TransferManager, nameof(TransferManager.RemainingSeconds)) {
				Converter = new SecondsCountToTimestampConverter()
			}.Bind();
			new BindingManager(SourceFolderTextBox, TextBox.TextProperty, this, nameof(SourceFolder)).Bind();
			new BindingManager(StopButton, Button.IsEnabledProperty, this, nameof(IsRunning)).Bind();
			new BindingManager(ListenButton, Button.IsEnabledProperty, this, nameof(IsListening)) {
				Converter = new BoolToInverseConverter()
			}.Bind();
			new BindingManager(DisableButton, Button.IsEnabledProperty, this, nameof(IsListening)).Bind();
			new BindingManager(AuthenticationCheckBox, CheckBox.IsEnabledProperty, this, nameof(IsListening)) {
				Converter = new BoolToInverseConverter()
			}.Bind();
		}

		private void UpdateActions() {
			ListenButton.Click += ListenButton_Click;
			DisableButton.Click += DisableButton_Click;
			StopButton.Click += StopButton_Click;
			BrowseButton.Click += BrowseButton_Click;
		}

		private void BrowseButton_Click(object sender, RoutedEventArgs e) {
			FolderBrowserDialog dialog = new FolderBrowserDialog();
			DialogResult result = dialog.ShowDialog();
			if (result == System.Windows.Forms.DialogResult.OK) {
				SourceFolder = dialog.SelectedPath;
			}
		}

		private void Server_Connected(object sender, TcpEventArgs e) {
			string sourceFolder = Dispatcher.Invoke(() => SourceFolder);
			try {
				Dispatcher.Invoke(() => IsRunning = true);
				TransferManager.Start(e.Stream, sourceFolder);
			} finally {
				Dispatcher.Invoke(() => IsRunning = false);
			}
		}

		private void StopButton_Click(object sender, RoutedEventArgs e) {
			IsRunning = false;
			Server.Stop();
		}

		private void ListenButton_Click(object sender, RoutedEventArgs e) {
			Settings.Default.SourceFolder = SourceFolder;
			Settings.Default.ServerHost = Host;
			Settings.Default.ServerPort = Port;
			Settings.Default.Save();

			IsListening = Server.IsListening();
			if (!IsListening) {
				bool confirmed = true;
				string username = null;
				SecureString password = null;
				if (IsAuthenticationEnabled) {
					AuthenticationView authView = new AuthenticationView();
					authView.Owner = this;
					authView.ShowDialog();
					username = authView.Username;
					password = authView.Password;
					confirmed = authView.IsConfirmed;
				}
				if (confirmed) Server.Listen(Host, Port, username, password);
			}
			IsListening = Server.IsListening();
		}

		private void DisableButton_Click(object sender, RoutedEventArgs e) {
			IsListening = Server.IsListening();
			if (IsListening) {
				Server.Disable();
			}
			IsListening = Server.IsListening();
		}
	}
}
