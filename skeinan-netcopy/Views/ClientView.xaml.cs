﻿using Skeinan.Netcopy.Connection;
using Skeinan.Netcopy.Properties;
using Skeinan.Netcopy.Transfer;
using SkeinanWpf;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using FolderBrowserDialog = System.Windows.Forms.FolderBrowserDialog;
using DialogResult = System.Windows.Forms.DialogResult;
using System.Text;

namespace Skeinan.Netcopy.Views {
	public partial class ClientView : Window {
		public static readonly DependencyProperty BackupFolderProperty =
				DependencyProperty.Register(nameof(BackupFolder), typeof(string), typeof(ClientView));
		public string BackupFolder {
			get { return (string)GetValue(BackupFolderProperty); }
			set { SetValue(BackupFolderProperty, value); }
		}

		public static readonly DependencyProperty HostProperty =
				DependencyProperty.Register(nameof(Host), typeof(string), typeof(ClientView));
		public string Host {
			get { return (string)GetValue(HostProperty); }
			set { SetValue(HostProperty, value); }
		}

		public static readonly DependencyProperty PortProperty =
				DependencyProperty.Register(nameof(Port), typeof(string), typeof(ClientView));
		public string Port {
			get { return (string)GetValue(PortProperty); }
			set { SetValue(PortProperty, value); }
		}

		public static readonly DependencyProperty ErrorMessagesProperty =
				DependencyProperty.Register(nameof(ErrorMessages), typeof(IEnumerable<string>), typeof(ClientView));
		public IEnumerable<string> ErrorMessages {
			get { return (IEnumerable<string>)GetValue(ErrorMessagesProperty); }
			set { SetValue(ErrorMessagesProperty, value); }
		}

		protected SkeinanLogger Logger { get; }
		protected SyncStorageUtil StorageUtil { get; }

		public ClientConnectionHandler Client { get; }
		public ClientTransferManager TransferManager { get; }

		public ClientView(SkeinanLogger logger, SyncStorageUtil storageUtil) {
			Logger = logger;
			StorageUtil = storageUtil;
			BackupFolder = Settings.Default.BackupFolder;
			Host = Settings.Default.ClientHost;
			Port = Settings.Default.ClientPort;
			Client = new ClientConnectionHandler(logger, storageUtil);
			TransferManager = new ClientTransferManager(logger, storageUtil);
			TransferManager.IsMd5Enabled = Settings.Default.IsMd5ComparisonEnabled;
			Client.Connected += Client_Connected;
			Client.AuthenticationRequired += Client_AuthenticationRequired;
			Client.CertificateChanged += Client_CertificateRejected;

			Client.ErrorMessages.ListChanged += ErrorMessages_ListChanged;
			TransferManager.ErrorMessages.ListChanged += ErrorMessages_ListChanged;

			InitializeComponent();
			UpdateBindings();
			UpdateActions();
		}

		private void Client_CertificateRejected(object sender, CertificateEventArgs e) {
			StringBuilder builder = new StringBuilder();
			builder.AppendLine("The certificate wasn't recognized. Proceed with the connection?");
			builder.AppendLine("Subject: " + e.Certificate.SubjectName.Name);
			builder.AppendLine("Issuer: " + e.Certificate.IssuerName.Name);
			builder.AppendLine("Thumbprint: " + e.Certificate.Thumbprint);
			e.IsCertificateAccepted = MessageBox.Show(builder.ToString(), "Invalid Certificate", MessageBoxButton.YesNo) == MessageBoxResult.Yes;
		}

		private void Client_AuthenticationRequired(object sender, AuthenticationEventArgs e) {
			Dispatcher.Invoke(() => {
				AuthenticationView authView = new AuthenticationView();
				authView.Owner = this;
				authView.ShowDialog();
				e.Username = authView.Username;
				e.Password = authView.Password;
			});
		}

		private void ErrorMessages_ListChanged(object sender, ListChangedEventArgs e) {
			Dispatcher.Invoke(MergeErrorMessages);
		}

		private void MergeErrorMessages() {
			ErrorMessages = Client.ErrorMessages.Concat(TransferManager.ErrorMessages);
		}

		private void UpdateBindings() {
			new BindingManager(HostTextBox, TextBox.TextProperty, this, nameof(Host)).Bind();
			new BindingManager(PortTextBox, TextBox.TextProperty, this, nameof(Port)).Bind();
			new BindingManager(ConnectionStatusLabel, Label.ContentProperty, Client, nameof(Client.Status)).Bind();
			new BindingManager(ConnectionLabel, Label.ContentProperty, Client, nameof(Client.Connection)).Bind();
			new BindingManager(LogLabel, Label.ContentProperty, this, nameof(ErrorMessages)) {
				Converter = new ListToStringConverter()
			}.Bind();
			new BindingManager(TransferStatusLabel, Label.ContentProperty, TransferManager, nameof(TransferManager.Status)).Bind();
			new BindingManager(TransferProgressLabel, Label.ContentProperty, TransferManager, nameof(TransferManager.Progress)) {
				Converter = new DoubleToStringConverter("%")
			}.Bind();
			new BindingManager(TransferSpeedLabel, Label.ContentProperty, TransferManager, nameof(TransferManager.MegaBytePerSecond)) {
				Converter = new DoubleToStringConverter(" MB/s")
			}.Bind();
			new BindingManager(RemainingTimeLabel, Label.ContentProperty, TransferManager, nameof(TransferManager.RemainingSeconds)) {
				Converter = new SecondsCountToTimestampConverter()
			}.Bind();
			new BindingManager(BackupFolderTextBox, TextBox.TextProperty, this, nameof(BackupFolder)).Bind();
		}

		private void UpdateActions() {
			ConnectButton.Click += ConnectButton_Click;
			BrowseButton.Click += BrowseButton_Click;
		}

		private void BrowseButton_Click(object sender, RoutedEventArgs e) {
			FolderBrowserDialog dialog = new FolderBrowserDialog();
			DialogResult result = dialog.ShowDialog();
			if (result == System.Windows.Forms.DialogResult.OK) {
				BackupFolder = dialog.SelectedPath;
			}
		}

		private void Client_Connected(object sender, TcpEventArgs e) {
			string backupFolder = Dispatcher.Invoke(() => BackupFolder);
			TransferManager.Start(e.Stream, backupFolder);
		}

		private void ConnectButton_Click(object sender, RoutedEventArgs e) {
			Settings.Default.BackupFolder = BackupFolder;
			Settings.Default.ClientHost = Host;
			Settings.Default.ClientPort = Port;
			Settings.Default.Save();
			Client.Connect(Host, Port);
		}
	}
}
