﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Skeinan.Netcopy {
	public class DoubleToStringConverter : IValueConverter {
		public string Unit { get; set; }

		public DoubleToStringConverter() {
		}

		public DoubleToStringConverter(string unit) {
			Unit = unit;
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			double? doubleValue = value as double?;
			string result = null;
			if (doubleValue != null) {
				result = ((double)doubleValue).ToString("0.00");
				result = result + Unit;
			}
			return result;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}
