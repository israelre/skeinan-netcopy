﻿using System.Collections.Generic;
using System.Linq;

namespace Skeinan.Netcopy.Transfer {
	public class FileChangeGroup {
		public long SizeToReceive {
			get {
				long totalSize = 0;
				FilesToReceive.ToList().ForEach(data => totalSize += data.Size);
				return totalSize;
			}
		}
		public IEnumerable<DetailedFile> FilesToRemove { get; set; }
		public IEnumerable<DetailedFile> FilesToChange { get; set; }
		public IEnumerable<DetailedFile> FilesToAdd { get; set; }
		public IEnumerable<DetailedFile> FilesToReceive {
			get {
				return FilesToAdd.Concat(FilesToChange);
			}
		}
	}
}
