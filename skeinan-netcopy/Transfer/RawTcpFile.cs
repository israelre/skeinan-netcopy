﻿namespace Skeinan.Netcopy.Transfer {
	public class RawTcpFile {
		public RawTcpFile(string filePath, byte[] data, bool isHeader) {
			FilePath = filePath;
			Data = data;
			IsHeader = isHeader;
		}

		public string FilePath { get; set; }
		public byte[] Data { get; set; }
		public bool IsHeader { get; set; }
	}
}
