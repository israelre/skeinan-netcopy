﻿using Skeinan.Netcopy.Connection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Skeinan.Netcopy.Transfer {
	public class ClientTransferManager : TransferManager {
		public SyncStorageUtil StorageUtil { get; }

		public ClientTransferManager(SkeinanLogger logger, SyncStorageUtil storageUtil) : base(logger) {
			StorageUtil = storageUtil;
		}

		public void Start(NetworkStreamWrapper stream, string backupFolder) {
			try {
				DateTime startTime = DateTime.Now;
				string outputPath = StorageUtil.GetBackupOutputPath(backupFolder);
				string trashPath = StorageUtil.GetBackupTrashPath(backupFolder, startTime);
				string addedLogPath = StorageUtil.GetBackupAddedLogPath(backupFolder, startTime);
				string changedLogPath = StorageUtil.GetBackupChangedLogPath(backupFolder, startTime);
				string removedLogPath = StorageUtil.GetBackupRemovedLogPath(backupFolder, startTime);

				ErrorMessages.Clear();
				
				Task<IEnumerable<DetailedFile>> remoteFileListTask = Task.Run(() => ReceiveFileData(stream));
				Task<IEnumerable<DetailedFile>> localFileListTask = Task.Run(() => GenerateFileData(outputPath));
				remoteFileListTask.Wait();
				localFileListTask.Wait();
				IEnumerable<DetailedFile> remoteFileList = remoteFileListTask.Result;
				IEnumerable<DetailedFile> localFileList = localFileListTask.Result;

				FileChangeGroup group = RetrieveFileChangeGroup(localFileList, remoteFileList, backupFolder, startTime);
				WriteBackupFileList(group, addedLogPath, changedLogPath, removedLogPath);
				CleanFiles(outputPath, trashPath, group.FilesToRemove.Concat(group.FilesToChange));

				long totalSize = group.SizeToReceive;
				RequestFiles(stream, totalSize, group.FilesToReceive, outputPath);
				ReceiveFiles(stream, totalSize, outputPath);

				while (stream.ReadBool()) {
					ErrorMessages.Add(stream.ReadString());
				}

				CalculateFinalStatus(totalSize);
			} catch (Exception ex) {
				Logger.LogError(ex);
				Status = "Failed";
				throw ex;
			}
		}

		private void RequestFiles(NetworkStreamWrapper stream, long totalSize, IEnumerable<DetailedFile> filesToReceive, string outputPath) {
			stream.WriteLong(totalSize);
			foreach (DetailedFile fileToReceive in filesToReceive) {
				RelativeFile relativeFile = new RelativeFile(fileToReceive.FileName);
				Status = "Requesting File: " + fileToReceive.FileName;
				if (relativeFile.ValidateRoot(outputPath)) {
					stream.WriteBool(true);
					stream.WriteString(fileToReceive.FileName);
				} else {
					ErrorMessages.Add("Invalid Path: " + fileToReceive);
				}
			}
			stream.WriteBool(false);
		}

		private void ReceiveFiles(NetworkStreamWrapper stream, long totalSize, string outputPath) {
			InitializeProgressCheck();
			while (stream.ReadBool()) {
				string filePath;
				filePath = stream.ReadString();
				RelativeFile relativeFile = new RelativeFile(filePath);
				Status = "Receiving File: " + filePath;
				if (relativeFile.ValidateRoot(outputPath)) {
					string absolutePath = relativeFile.GetAbsolutePath(outputPath);
					stream.ReadFile(absolutePath, (bytesCount) => CalculateProgress(bytesCount, totalSize));
				} else {
					ErrorMessages.Add("Invalid path: " + filePath);
					stream.SkipFile((bytesCount) => CalculateProgress(bytesCount, totalSize));
				}
			}
			FinalizeProgressCheck();
		}

		private IEnumerable<DetailedFile> ReceiveFileData(NetworkStreamWrapper stream) {
			HashSet<DetailedFile> remoteFileDataList = new HashSet<DetailedFile>();

			while (stream.ReadBool()) {
				DetailedFile remoteFile = ReadFileComparisonData(stream);
				Status = "Receiving File Data: " + remoteFile.FileName;
				remoteFileDataList.Add(remoteFile);
			}
			return remoteFileDataList;
		}

		private IEnumerable<DetailedFile> GenerateFileData(string outputPath) {
			HashSet<DetailedFile> localFileDataList = new HashSet<DetailedFile>();
			foreach (DetailedFile fileData in GenerateFileComparisonData(outputPath)) {
				Status = "Preparing File Data: " + fileData.FileName;
				localFileDataList.Add(fileData);
			}
			return localFileDataList;
		}

		private FileChangeGroup RetrieveFileChangeGroup(IEnumerable<DetailedFile> localFiles, IEnumerable<DetailedFile> remoteFiles, string outputPath, DateTime startTime) {
			IEnumerable<DetailedFile> filesToReceive = remoteFiles.Except(localFiles);

			FileChangeGroup group = new FileChangeGroup();
			group.FilesToRemove = RetrieveFilesToRemove(localFiles, remoteFiles);
			group.FilesToChange = filesToReceive.Where(file => File.Exists(new RelativeFile(file.FileName).GetAbsolutePath(outputPath)));
			group.FilesToAdd = filesToReceive.Except(group.FilesToChange);

			return group;
		}

		private IEnumerable<DetailedFile> RetrieveFilesToRemove(IEnumerable<DetailedFile> localFiles, IEnumerable<DetailedFile> remoteFiles) {
			IEnumerable<string> localFilePaths = localFiles.Select(item => item.FileName);
			IEnumerable<string> remoteFilePaths = remoteFiles.Select(item => item.FileName);
			return localFiles.Where(file => !remoteFilePaths.Contains(file.FileName));
		}

		private void WriteBackupFileList(FileChangeGroup group, string addedLogPath, string changedLogPath, string removedLogPath) {
			WriteBackupFileList(group.FilesToAdd, addedLogPath);
			WriteBackupFileList(group.FilesToChange, changedLogPath);
			WriteBackupFileList(group.FilesToRemove, removedLogPath);
		}

		private void WriteBackupFileList(IEnumerable<DetailedFile> files, string path) {
			if (files.Any()) {
				new AbsoluteFile(path).WriteAllLines(files.Select(file => file.FileName));
			}
		}

		private void CleanFiles(string outputPath, string trashPath, IEnumerable<DetailedFile> filesToArchive) {
			if (Directory.Exists(outputPath)) {
				foreach (DetailedFile fileToArchive in filesToArchive) {
					new RelativeFile(fileToArchive.FileName).Move(outputPath, trashPath);
				}

				foreach (string currentDir in Directory.EnumerateDirectories(outputPath, "*", SearchOption.AllDirectories)) {
					if (Directory.GetFiles(currentDir, "*", SearchOption.AllDirectories).Length == 0) {
						Directory.Delete(currentDir, true);
					}
				}
			}
		}
	}
}
