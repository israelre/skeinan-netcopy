﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;

namespace Skeinan.Netcopy.Transfer {
	public class AbsoluteFile {
		public string FilePath { get; }

		public AbsoluteFile(string filePath) {
			FilePath = filePath;
		}

		private string FixPath(string path, bool isDirectory) {
			string newPath = path.Replace("\\", "/");
			if (isDirectory && !newPath.EndsWith("/")) {
				newPath += "/";
			}
			return newPath;
		}

		public string GetRelativePath(string rootPath) {
				Uri uri = new Uri(FilePath);
				return Uri.UnescapeDataString(new Uri(FixPath(rootPath, true)).MakeRelativeUri(uri).ToString());
		}

		public string GenerateMd5() {
			using (MD5 md5 = MD5.Create()) {
				if (File.Exists(FilePath)) {
					using (FileStream stream = File.OpenRead(FilePath)) {
						byte[] bytes = md5.ComputeHash(stream);
						return BitConverter.ToString(bytes).Replace("-", "").ToLowerInvariant();
					}
				} else {
					return null;
				}
			}
		}

		public long CalculateSize() {
			return new FileInfo(FilePath).Length;
		}

		public void WriteAllLines(IEnumerable<string> content) {
			new FileInfo(FilePath).Directory.Create();
			File.WriteAllLines(FilePath, content);
		}
	}
}
