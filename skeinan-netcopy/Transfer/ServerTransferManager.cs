﻿using Skeinan.Netcopy.Connection;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Skeinan.Netcopy.Transfer {
	public class ServerTransferManager : TransferManager {
		private const int FILE_BUFFER_SIZE = 10485760;
		private const int MAX_QUEUE_SIZE = 20;
		private volatile bool mIsPendingDataComplete;
		private ConcurrentQueue<RawTcpFile> mPendingData = new ConcurrentQueue<RawTcpFile>();

		public ServerTransferManager(SkeinanLogger logger) : base(logger) {
		}

		public void Start(NetworkStreamWrapper stream, string sourceFolder) {
			try {
				ErrorMessages.Clear();
				WriteMd5Phase(stream, sourceFolder);

				Status = "Awaiting File Request";

				InitializeProgressCheck();
				long totalSize = stream.ReadLong();
				List<string> filePaths = new List<string>();
				while (stream.ReadBool()) {
					string filePath = stream.ReadString();
					filePaths.Add(filePath);
				}

				mIsPendingDataComplete = false;
				Task readFilesTask = Task.Run(() => ReadFiles(sourceFolder, filePaths));
				Task sendFilesTask = Task.Run(() => SendFiles(stream, totalSize));
				readFilesTask.Wait();
				sendFilesTask.Wait();
				stream.WriteBool(false);
				FinalizeProgressCheck();

				foreach (string message in ErrorMessages) {
					stream.WriteBool(true);
					stream.WriteString(message);
				}
				stream.WriteBool(false);

				CalculateFinalStatus(totalSize);
			} catch (Exception ex) {
				Logger.LogError(ex);
				Status = "Failed";
				throw ex;
			}
		}

		private void ReadFiles(string sourceFolder, List<string> filePaths) {
			foreach (string filePath in filePaths) {
				string absolutePath = new RelativeFile(filePath).GetAbsolutePath(sourceFolder);
				if (File.Exists(absolutePath)) {
					while (mPendingData.Count >= MAX_QUEUE_SIZE) {
						Thread.Sleep(100);
					}
					mPendingData.Enqueue(new RawTcpFile(filePath, new byte[] { NetworkStreamWrapper.ConvertToByte(true) }, true));
					mPendingData.Enqueue(new RawTcpFile(filePath, NetworkStreamWrapper.ConvertToBytes(filePath), true));
					long fileSize = new FileInfo(absolutePath).Length;
					mPendingData.Enqueue(new RawTcpFile(filePath, NetworkStreamWrapper.ConvertToBytes(fileSize), true));
					using (FileStream fileStream = new FileStream(absolutePath, FileMode.Open)) {
						long remainingSize = fileSize;
						while (remainingSize > 0) {
							long currentSize = Math.Min(remainingSize, FILE_BUFFER_SIZE);
							byte[] bytes = new byte[currentSize];
							remainingSize -= fileStream.Read(bytes, 0, bytes.Length);
							mPendingData.Enqueue(new RawTcpFile(filePath, bytes, false));
						}
					}
				} else {
					ErrorMessages.Add("File not found: " + filePath);
				}
			}
			mIsPendingDataComplete = true;
		}

		private void SendFiles(NetworkStreamWrapper stream, long totalSize) {
			while (!(mIsPendingDataComplete && mPendingData.IsEmpty)) {
				if (mPendingData.IsEmpty) {
					Thread.Sleep(10);
				} else {
					mPendingData.TryDequeue(out RawTcpFile file);
					Status = "Sending File: " + file.FilePath;
					stream.WriteBytes(file.Data);
					if (!file.IsHeader) CalculateProgress(file.Data.Length, totalSize);
				}
			}
		}

		private void WriteMd5Phase(NetworkStreamWrapper stream, string sourceFolder) {
			foreach (DetailedFile md5Entry in GenerateFileComparisonData(sourceFolder)) {
				Status = "Sending File Data: " + md5Entry.FileName;
				stream.WriteBool(true);
				stream.WriteString(md5Entry.FileName);
				stream.WriteString(md5Entry.Md5);
				stream.WriteLong(md5Entry.Size);
			}
			stream.WriteBool(false);
		}
	}
}
