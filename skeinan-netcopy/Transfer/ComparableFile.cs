﻿namespace Skeinan.Netcopy.Transfer {
	public class DetailedFile {
		public string FileName { get; set; }
		public string Md5 { get; set; }
		public long Size { get; set; }

		public DetailedFile(string fileName, string md5, long size) {
			FileName = fileName;
			Md5 = md5;
			Size = size;
		}

		public override bool Equals(object obj) {
			return obj is DetailedFile targetObj &&
					Equals(FileName, targetObj.FileName) &&
					Equals(Md5, targetObj.Md5)
					&& Size == targetObj.Size;
		}

		public override int GetHashCode() {
			int hash = 17;
			hash = hash * 23 + FileName.GetHashCode();
			hash = hash * 23 + Md5.GetHashCode();
			hash = hash * 23 + Size.GetHashCode();
			return hash;
		}
	}
}
