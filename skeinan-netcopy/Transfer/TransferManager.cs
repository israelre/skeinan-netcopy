﻿using Skeinan.Netcopy.Connection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;

namespace Skeinan.Netcopy.Transfer {
	public class TransferManager : Service {
		private const int PROGRESS_INTERVAL_MILLIS_COUNT = 1000;
		private const int PROGRESS_INITIAL_MILLIS_COUNT = 100;
		private const int PROGRESS_TIME_REFRESH_MILLIS_COUNT = 100;
		private const double PROGRESS_SMOOTHING_FACTOR = 0.05;

		private DateTime mSpeedCheckTimestamp;
		private long mSpeedCheckBytesCount;
		private long mReceivedSize;
		protected SkeinanLogger Logger { get; }

		private bool mIsMd5Enabled;
		public bool IsMd5Enabled {
			get { return mIsMd5Enabled; }
			set { mIsMd5Enabled = value; NotifyPropertyChanged(); }
		}

		private double? mProgress;
		public double? Progress {
			get { return mProgress; }
			set { mProgress = value; NotifyPropertyChanged(); }
		}

		private double? mMegaBytePerSecond;
		public double? MegaBytePerSecond {
			get { return mMegaBytePerSecond; }
			set { mMegaBytePerSecond = value; NotifyPropertyChanged(); }
		}

		private double? mRemainingSeconds;
		public double? RemainingSeconds {
			get { return mRemainingSeconds; }
			set { mRemainingSeconds = value; NotifyPropertyChanged(); }
		}

		private string mStatus;
		public string Status {
			get { return mStatus; }
			set { mStatus = value; NotifyPropertyChanged(); }
		}

		private BindingList<string> mErrorMessages = new BindingList<string>();
		public BindingList<string> ErrorMessages {
			get { return mErrorMessages; }
			set { mErrorMessages = value; NotifyPropertyChanged(); }
		}

		public TransferManager(SkeinanLogger logger) {
			Logger = logger;
		}

		protected DetailedFile ReadFileComparisonData(NetworkStreamWrapper stream) {
			string fileName = stream.ReadString();
			string md5 = stream.ReadString();
			long size = stream.ReadLong();
			return new DetailedFile(fileName, md5, size);
		}

		protected IEnumerable<DetailedFile> GenerateFileComparisonData(string folderPath) {
			if (Directory.Exists(folderPath)) {
				foreach (string currentFile in Directory.EnumerateFiles(folderPath, "*", SearchOption.AllDirectories)) {
					AbsoluteFile absoluteFile = new AbsoluteFile(currentFile);
					string relativePath = absoluteFile.GetRelativePath(folderPath);
					string md5 = IsMd5Enabled ? absoluteFile.GenerateMd5() : "";
					long size = absoluteFile.CalculateSize();
					yield return new DetailedFile(relativePath, md5, size);
				}
			}
		}

		protected void InitializeProgressCheck() {
			mSpeedCheckTimestamp = DateTime.Now;
			mSpeedCheckBytesCount = 0;
			mReceivedSize = 0;
		}

		protected void CalculateProgress(long bytesCount, long totalSize) {
			mSpeedCheckBytesCount += bytesCount;
			double timeDifference = DateTime.Now.Subtract(mSpeedCheckTimestamp).TotalMilliseconds;
			bool firstCalculation = MegaBytePerSecond == null && timeDifference >= PROGRESS_INITIAL_MILLIS_COUNT;
			if (timeDifference >= PROGRESS_INTERVAL_MILLIS_COUNT || firstCalculation) {
				double currentMbPerSecond = (double)mSpeedCheckBytesCount * 1000 / (1024 * 1024 * timeDifference);
				mReceivedSize += mSpeedCheckBytesCount;
				mSpeedCheckBytesCount = 0;
				Progress = (double)mReceivedSize * 100 / totalSize;
				if (MegaBytePerSecond == null || MegaBytePerSecond == 0) {
					MegaBytePerSecond = currentMbPerSecond;
				} else {
					MegaBytePerSecond = PROGRESS_SMOOTHING_FACTOR * currentMbPerSecond + (1 - PROGRESS_SMOOTHING_FACTOR) * MegaBytePerSecond;
				}
				mSpeedCheckTimestamp = DateTime.Now;
			}
			if (timeDifference >= PROGRESS_TIME_REFRESH_MILLIS_COUNT || firstCalculation) {
				RemainingSeconds = (totalSize - mReceivedSize) / (1024 * 1024 * MegaBytePerSecond);
			}
		}

		protected void FinalizeProgressCheck() {
			MegaBytePerSecond = null;
			RemainingSeconds = null;
			Progress = null;
		}

		protected void CalculateFinalStatus(long totalByteCount) {
			if (ErrorMessages.Count > 0) {
				Status = "Completed With Errors";
			} else if (totalByteCount > 0) {
				Status = "Completed";
			} else {
				Status = "Completed Without Changes";
			}
		}
	}
}
