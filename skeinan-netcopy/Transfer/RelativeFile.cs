﻿using System;
using System.IO;

namespace Skeinan.Netcopy.Transfer {
	public class RelativeFile {
		public string FilePath { get; }

		public RelativeFile(string filePath) {
			FilePath = filePath;
		}

		private string FixPath(string path, bool isDirectory) {
			string newPath = path.Replace("\\", "/");
			if (isDirectory && !newPath.EndsWith("/")) {
				newPath += "/";
			}
			return newPath;
		}

		public string GetAbsolutePath(string rootPath) {
			Uri baseUri = new Uri(FixPath(rootPath, true));
			Uri uri = new Uri(baseUri, FixPath(FilePath, false));
			return Uri.UnescapeDataString(uri.AbsolutePath);
		}

		public bool ValidateRoot(string root) {
			return ValidateRoot(GetAbsolutePath(root), root);
		}

		private bool ValidateRoot(string folder, string root) {
			return ValidateRoot(new DirectoryInfo(folder), new DirectoryInfo(root));
		}

		private bool ValidateRoot(DirectoryInfo folder, DirectoryInfo parent) {
			if (folder.Parent == null) {
				return false;
			} else if (string.Equals(FixPath(folder.Parent.FullName, true), FixPath(parent.FullName, true), StringComparison.InvariantCultureIgnoreCase)) {
				return true;
			} else {
				return ValidateRoot(folder.Parent, parent);
			}
		}

		public void Copy(string rootPath, string targetPath) {
			string targetFilePath = GetAbsolutePath(targetPath);
			new FileInfo(targetFilePath).Directory.Create();
			File.Copy(GetAbsolutePath(rootPath), targetFilePath);
		}

		public void Move(string rootPath, string targetPath) {
			string targetFilePath = GetAbsolutePath(targetPath);
			new FileInfo(targetFilePath).Directory.Create();
			File.Move(GetAbsolutePath(rootPath), targetFilePath);
		}
	}
}
