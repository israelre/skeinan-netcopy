﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace Skeinan.Netcopy {
	public class ListToStringConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			IEnumerable<string> listValue = value as IEnumerable<string>;
			return string.Join(Environment.NewLine, listValue ?? new List<string>());
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}
