﻿using Skeinan.Netcopy.Properties;
using Skeinan.Netcopy.Views;
using System.Windows;

namespace Skeinan.Netcopy
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application {
		private void Application_Startup(object sender, StartupEventArgs e) {
			SkeinanLogger logger = new SkeinanLogger("C:/Users/Israel/Downloads/Skeinan.log");
			SyncStorageUtil storageUtil = new SyncStorageUtil();
			if (Settings.Default.IsServerEnabled) {
				new ServerView(logger, storageUtil).Show();
			}
			if (Settings.Default.IsClientEnabled) {
				new ClientView(logger, storageUtil).Show();
			}
		}
	}
}
